<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Header;
use App\Row;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
	/* View Student Registration Form
	*/
    public function addStudent(){
    	$headers = Header::all();
    	return view('student.add', ['headers' => $headers]);
    }

    /*
    * Submit the student registration form
    * $request 
    */

    public function saveStudent(Request $request){
    	$data = $request->all();
    	Row::create([
            'values' => $data,
        ]);
        return redirect('home');
    }

    /*
    * View Edit Form for Students
    */
    public function editStudent($id){
    	$data = Row::whereRowId($id)->first();
    	$headers = Header::all();
    	return View('student.edit', ['data'=>$data->values, 'headers' => $headers, 'id'=> $id]);
    }

    /*
    * Delete the student record from the database
    */

    public function deleteStudent($id){
    	$data = Row::whereRowId($id)->delete();
    	return redirect('home');
    }

    /*
    * Update the existing record of the student
    */
     public function updateStudent(Request $request, $id){

     	$data = json_encode($request->all());
     	DB::table('rows')
                ->where('row_id', $id)
                ->update(['values' => $data]);
     	return redirect('home');
    }
}
