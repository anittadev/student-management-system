<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Row;
use App\Header;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = Row::all();
        $headers = Header::all();
        return view('home', ['rows'=>$rows,'headers'=>$headers]);
    }
}
