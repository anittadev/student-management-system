<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Row extends Model
{
    protected $fillable = [
        'values'
    ];

    protected $casts = [
        'values' => 'array',
    ];
}
