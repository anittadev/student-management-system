
@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Student Registration</h2></div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('save_student') }}">
                        {{ csrf_field() }}

                        @foreach ($headers as $header)
	                        <div class="form-group{{ $errors->has($header->header_id) ? ' has-error' : '' }}">
	                            <label for="{{ $header->header_id }}" class="col-md-4 control-label">{{ $header->attribute }}</label>

	                            <div class="col-md-6">
	                                <input id="{{ $header->header_id }}" type="text" class="form-control" name="{{ $header->header_id }}" required autofocus>	                               
	                            </div>
	                        </div>
						@endforeach
						<div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    REGISTER
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
