@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Student Management</h2></div>

                <!-- <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div> -->

                <table>
                  <thead>
                <tr>
                    @foreach($headers as $header)
                     <th scope="col">{{ $header->attribute}}</th>
                    @endforeach
                    <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($rows as $row)
                <tr>
                     @foreach($headers as $header)
                         <td data-label="Account">{{ $row->values[$header->header_id]}}</td>
                     @endforeach
                     <td>
                         <a href="{{ route('edit_student',$row->row_id) }}">Edit</a> / <a href="{{ route('delete_student',$row->row_id) }}">Delete </a>
                     </td>
                </tr>

                @endforeach

              </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
@endsection
