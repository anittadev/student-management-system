<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/student/add', 'StudentController@addStudent')->name('add_student');
Route::post('/student/save', 'StudentController@saveStudent')->name('save_student');
Route::get('/student/{id}/edit', 'StudentController@editStudent')->name('edit_student');
Route::get('/student/{id}/delete', 'StudentController@deleteStudent')->name('delete_student');
Route::post('/student/{id}/update', 'StudentController@updateStudent')->name('update_student');